import unittest

from main import getInformationOfPaper

testPaper = [
    ('10.1145/3158661', 'article-journal'),
    ('10.1109/cloud.2017.89', 'paper-conference'),
    ('10.1109/access.2019.2943498', 'article-journal'),
    ('10.1109/ccem.2017.23', 'paper-conference'),
    ('10.1002/spe.2409', 'article-journal'),
    ('10.1007/s10586-017-1248-y', 'article-journal'),
    ('10.12694/scpe.v16i4.1134', 'article-journal'),
    ('10.1186/2192-113x-2-12', 'article-journal'),
    ('10.1109/jcdl.2014.6970216', 'paper-conference'),
    ('10.4018/978-1-4666-8629-8.ch011', 'chapter'),
    ('10.1007/978-3-319-46031-4', 'book'),
    ('10.1007/978-3-319-31861-5_7', 'chapter'),
    ('10.1007/s00607-014-0421-x', 'article-journal'),
    ('10.1007/978-3-319-38904-2_2', 'chapter'),
    ('10.1007/s13369-015-1703-0', 'article-journal'),
    ('10.1007/s10515-014-0143-5', 'article-journal'),
    ('10.5121/ijnsa.2014.6103', 'article-journal'),
    ('10.1109/tgrs.2015.2424719', 'article-journal'),
    ('10.1145/2659651.2659735', 'paper-conference'),
    ('10.5220/0005804301380145', 'paper-conference'),
    ('10.1007/3-540-63697-8_107', 'chapter'),
    ('10.1016/s0143-974x(99)00037-1', 'article-journal'),
    ('10.1007/978-3-540-93964-1_3', 'chapter'),
    ('10.1016/j.eswa.2010.10.070', 'article-journal'),
    ('10.1016/j.cor.2010.06.007', 'article-journal'),
    ('10.1145/2330163.2330341', 'paper-conference'),
    ('10.1109/sde.2013.6601435', 'paper-conference'),
    ('10.3390/a11.339', 'article-journal')
]


class TestPaperTypeForAll(unittest.TestCase):
    def testPaperType0(self):
        paper = testPaper[0]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType1(self):
        paper = testPaper[1]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType2(self):
        paper = testPaper[2]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType3(self):
        paper = testPaper[3]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType4(self):
        paper = testPaper[4]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType5(self):
        paper = testPaper[5]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType6(self):
        paper = testPaper[6]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType7(self):
        paper = testPaper[7]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType8(self):
        paper = testPaper[8]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType9(self):
        paper = testPaper[9]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType10(self):
        paper = testPaper[10]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType11(self):
        paper = testPaper[11]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType12(self):
        paper = testPaper[12]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType13(self):
        paper = testPaper[13]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType14(self):
        paper = testPaper[14]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType15(self):
        paper = testPaper[15]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType16(self):
        paper = testPaper[16]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType17(self):
        paper = testPaper[17]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType18(self):
        paper = testPaper[18]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType19(self):
        paper = testPaper[19]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType20(self):
        paper = testPaper[20]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType21(self):
        paper = testPaper[21]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType22(self):
        paper = testPaper[22]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType23(self):
        paper = testPaper[23]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType24(self):
        paper = testPaper[24]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType25(self):
        paper = testPaper[25]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])

    def testPaperType26(self):
        paper = testPaper[26]
        paperInformation = getInformationOfPaper(paper[0])
        self.assertEqual(paperInformation['type'], paper[1])


if __name__ == '__main__':
    unittest.main()
