import os
import json
from math import ceil
from operator import itemgetter

files = []
# r=root, d=directories, f = files
for r, d, f in os.walk('Files'):
    for file in f:
        files.append(file)

print(files)

fscie = []
fssci = []

for i in files:
    if 'SCIE' in i:
        fscie.append(i)
    elif 'SSCI' in i:
        fssci.append(i)
print(fscie)
print(fssci)
f1 = []  # extracted years from fscie
for i in fscie:
    f1.append(int(i[19:23]))
#print(f1)  # asd
f2 = []  # extracted years from fssci
for i in fssci:
    f2.append(int(i[19:23]))
#print(f2)  # asd

#year = '1900'  # asdf
closest_scie = str(min(f1, key=lambda x: abs(x - int(year))))
print(closest_scie)  # asd
closest_ssci = str(min(f2, key=lambda x: abs(x - int(year))))
print(closest_ssci)  # asd

rfscie = [i for i in fscie if closest_scie in i][-1]  # relevant file scie
print(rfscie)  # asd
rfssci = [i for i in fssci if closest_ssci in i][-1]  # relevant file ssci
print(rfssci)  # asd

os.chdir('Files')
# print(os.listdir())
# print(os.getcwd())
data = json.load(open(rfscie))
# issn = '1063-7710'  # asd
issn = '1061-1959'  # asd
nf = True  # not found
for i in data:
    if 'issn' in i and i['issn'] == issn:
        journal = {'journalImpactFactor': i['journalImpactFactor'],
                   'articleInfluenceScore': i['articleInfluenceScore'],
                   'categoryName': i['categoryName']}
        nf = False
        break
if nf:
    data = json.load(open(rfssci))
    for i in data:
        if 'issn' in i and i['issn'] == issn:
            journal = {'journalImpactFactor': i['journalImpactFactor'],
                       'articleInfluenceScore': i['articleInfluenceScore'],
                       'categoryName': i['categoryName']}
        break

print(journal)  # asd
l = []
for i in data:
    if 'categoryName' in i and i['categoryName'] == journal['categoryName']:
        dic = {'issn': i['issn'],
               'journalImpactFactor': i['journalImpactFactor'],
               'articleInfluenceScore': i['articleInfluenceScore']}
        l.append(dic)
print(l)  # asd

l = sorted(l, key=itemgetter('journalImpactFactor'), reverse=True)
print(l)  # asd
print(len(l))  # asd

for i in range(len(l)):
    if l[i]['issn'] == issn:
        rank = i
        break
print(rank)
# print(l.index(filter(lambda x: x.get('issn') == issn, l)))
container_title = "asdsd"
classCNATDCU = ''

if rank <= ceil(0.25 * len(l)):
    classCNATDCU = 'ISI ROSU'
    if 'NATURE' in container_title:
        classCNATDCU = 'NATURE'
elif rank <= ceil(0.5 * len(l)):
    print('sad')
    if not classCNATDCU == 'ISI ROSU':
        classCNATDCU = 'ISI GALBEN'
elif not classCNATDCU == 'ISI ROSU' or not classCNATDCU == 'ISI GALBEN':
    classCNATDCU = 'ISI ALB'

print(classCNATDCU)

