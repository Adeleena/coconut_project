# Team Coconut
Adelina-Daniela Aursulesei, Csaba Püsök, Alexandru-Robert Zbiera; students of West University of Timișoara

# Coconut project
This project has the intention of detecting the type of publication not yet possible in URAP systems

### Requirements

* Python 3
* Libraries Used
    * [PycURL](http://pycurl.io/)  - used to make GET requests  
    * [OpenPyXL](https://openpyxl.readthedocs.io/en/stable/#) - used to read xlsx
* Folder 'Files' in the root folder, containing csv, json and xlsx with information about publications 

### How to run the code:
Running main.py can be done in two ways:

* Console mode, where you have pass two required arguments: 
    * DOI - the Digital Object Identifier of the publication
    * INFO - boolean values that describes if the publication is from Computer Science **(Accepts: yes/y/no/n)**
* Batch mode, where you need to provide a file called Test-Data.txt in the folder Files from the root folder; the files should contain a list of DOIs, one DOI per line, no empty lines 

**NOTE** The Batch mode executes as the console mode for all the DOIs with INFO as true **NOTE**

The result is a dictionary containing the following properties of the publication: type, classCNATDCU, WoS and classINFO