import curlHelper
import json

#print info related to doi

# doi = '10.1007/978-3-540-93964-1_3'
# doi = '10.4018/978-1-4666-8629-8.ch011'
doi = '10.1002/cpe.3421'
# doi = '10.1109/cloud.2017.89'
a = curlHelper.CurlHelper()
z = a.getOutputText(doi)

print(json.dumps(z, indent=4, sort_keys=True))