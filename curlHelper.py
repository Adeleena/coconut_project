import json
from io import StringIO

import pycurl

from exceptions import *

WOS_LINK = 'http://ws.isiknowledge.com/cps/openurl/service?url_ver=Z39.88-2004&rft_id=info:doi/{}'
OUTPUT_TEXT_LINK = 'http://dx.doi.org/{}'
SCOPUS_LINK = 'https://plu.mx/api/v1/artifact/doi/{}'


def __getWOSFromHeader(headerLine, writer):
    decodedHeaderLine = headerLine.decode('iso-8859-1')

    if 'WOS:' not in decodedHeaderLine:
        return None
    else:
        startOfWOS = decodedHeaderLine.index('WOS:') + 4
        endOfWOS = decodedHeaderLine[startOfWOS:].index('&')
        writer.write(decodedHeaderLine[startOfWOS: startOfWOS + endOfWOS])


def __getOutputBodyText(outputText, writer):
    writer.write(outputText.decode('iso-8859-1'))


class CurlHelper:
    """
    A class to be used to simplify the GET requests needed for the project
    """

    def __init__(self):
        self.curl = pycurl.Curl()

    def getWOS(self, doi: str) -> str or None:
        """
        Helper method that returns the WOS of the paper if found
        :param doi: Digital Object Identifier of the publication
        :return: the WOS of the publication, or None if no WOS was found
        :exception RequestException - for when the request fails
        """
        headers = StringIO()
        self.curl.setopt(self.curl.URL, WOS_LINK.format(doi))
        self.curl.setopt(self.curl.HEADER, 0)
        self.curl.setopt(self.curl.NOBODY, 1)
        self.curl.setopt(self.curl.HEADERFUNCTION, lambda h: __getWOSFromHeader(h, headers))
        try:
            self.curl.perform()
            return headers.getvalue()
        except pycurl.error:
            raise RequestException(RequestException.formatMessage(WOS_LINK.format(doi)))

    def __getBodyFromGetRequest(self, url: str) -> object:
        """
        private method used to get the json body from a get request
        :param url: the url of the request
        :return: the json body parsed
        :exception RequestException - for when the request fails
        :exception JSONException - for when the response is not JSON, or the JSON is flawed
        """
        objectBodyText = StringIO()
        self.curl.setopt(self.curl.URL, url)
        self.curl.setopt(self.curl.HTTPHEADER, ['Accept: application/citeproc+json'])
        self.curl.setopt(self.curl.FOLLOWLOCATION, True)
        self.curl.setopt(self.curl.HEADER, 0)
        self.curl.setopt(self.curl.NOBODY, 0)
        self.curl.setopt(self.curl.SSL_VERIFYPEER, 0)
        self.curl.setopt(self.curl.SSL_VERIFYHOST, 0)
        self.curl.setopt(self.curl.WRITEFUNCTION, lambda o: __getOutputBodyText(o, objectBodyText))
        try:
            self.curl.perform()
            return json.loads(objectBodyText.getvalue())
        except pycurl.error:
            raise RequestException(RequestException.formatMessage(url))
        except json.decoder.JSONDecodeError:
            raise JSONException(JSONException.formatMessage(objectBodyText.getvalue()))

    def getOutputText(self, doi: str) -> object:
        """
        Helper method that returns the data of the publication from dx.doi.org
        :param doi: Digital Object Identifier of the publication
        :return: the data of the publication from dx.doi.org
        :exception RequestException - for when the request fails
        :exception JSONException - for when the response is not JSON, or the JSON is flawed
        """
        return self.__getBodyFromGetRequest(OUTPUT_TEXT_LINK.format(doi))

    def getScopusObject(self, doi: str) -> object:
        """
        Helper method that returns the data of the publication from plu.mx/api/v1/artifact/doi/
        :param doi: Digital Object Identifier of the publication
        :return: the data of the publication from plu.mx/api/v1/artifact/doi/
        :exception RequestException - for when the request fails
        :exception JSONException - for when the response is not JSON, or the JSON is flawed
        """
        return self.__getBodyFromGetRequest(SCOPUS_LINK.format(doi))
