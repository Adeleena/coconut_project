# Created by Adelina-Daniela Aursulesei, Csaba Püsök, Alexandru-Robert Zbiera
# West University of Timișoara


import json
import os
import sys
from math import ceil, floor
from curlHelper import CurlHelper
from exceptions import *
from logger import Logger
from operator import itemgetter
import csv
from difflib import SequenceMatcher
import openpyxl

curlHelper = CurlHelper()
showInfoLogger = Logger(showInfo=True)


def getClass(journal, data, container_title, issn, sort, INFO):
    classCNATDCU = ''
    v = 3 # it is used to compare CNATDCU classifications
    classINFO = 'D'
    vi = 4 # it is used to compare INFO classifications
    l = []
    ranks = []

    if len(journal) != 0:
        for j in journal:
            for i in data:
                if 'categoryName' in i and i['categoryName'] == j['categoryName']:
                    dic = {'issn': i['issn'],
                           'journalImpactFactor': i['journalImpactFactor'],
                           'articleInfluenceScore': i['articleInfluenceScore']}
                    l.append(dic) # creates a list of articles that are in the same category

            l = sorted(l, key=itemgetter(sort), reverse=True) # sorts the list in descending order

            for i in range(len(l)):
                if l[i]['issn'] == issn:
                    ranks.append(i)
                    break # create a list of potential ranks based on the categories

        rank = min(ranks) # best rank
        if rank <= ceil(0.25 * len(l)):
            classCNATDCU = 'ISI ROSU'
            v = 0
            if 'NATURE' in container_title:
                classCNATDCU = 'NATURE'
        elif rank <= ceil(0.5 * len(l)):
            if not classCNATDCU == 'ISI ROSU':
                classCNATDCU = 'ISI GALBEN'
                v = 1
        elif not classCNATDCU == 'ISI ROSU' or not classCNATDCU == 'ISI GALBEN':
            classCNATDCU = 'ISI ALB'
            v = 2
        if INFO:
            x = floor(0.2 * ceil(0.25 * len(l)))
            if rank <= x:
                classINFO = 'A*'
                vi = 0
            elif rank <= ceil(0.25 * len(l)) + x:
                if classINFO != 'A*':
                    classINFO = 'A'
                    vi = 1
            elif rank <= ceil(0.5 * len(l)) + x:
                if classINFO != 'A*' or classINFO != 'A':
                    classINFO = "B"
                    vi = 2
            elif classINFO != 'A*' or classINFO != 'A' or classINFO != 'B':
                classINFO = 'C'
                vi = 3
    return [classCNATDCU, classINFO, v, vi]


def getClassCNATDCUFromArticleJournal(INFO, issn, year, container_title, wos, doi):
    if type(issn) == list:
        issn = issn[0]
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk('Files'):
        for file in f:
            files.append(file)

    fscie = [] # file names that contain SCIE
    fssci = [] # file names that contain SSCI

    for i in files:
        if 'SCIE' in i:
            fscie.append(i)
        elif 'SSCI' in i:
            fssci.append(i)
    f1 = []  # extracted years from fscie
    for i in fscie:
        f1.append(int(i[19:23]))
    f2 = []  # extracted years from fssci
    for i in fssci:
        f2.append(int(i[19:23]))
    closest_scie = str(min(f1, key=lambda x: abs(x - int(year))))
    closest_ssci = str(min(f2, key=lambda x: abs(x - int(year))))

    rfscie = [i for i in fscie if closest_scie in i][0]  # relevant file scie
    rfssci = [i for i in fssci if closest_ssci in i][0]  # relevant file ssci

    os.chdir('Files')
    rfscieFile = open(rfscie)
    data = json.load(rfscieFile)
    rfscieFile.close()
    journal = []
    for i in data:
        if 'issn' in i and i['issn'] == issn:
            journal.append({'journalImpactFactor': i['journalImpactFactor'],
                            'articleInfluenceScore': i['articleInfluenceScore'],
                            'categoryName': i['categoryName']})
            # gets all the instances of the article that can appear in multiple categories

    if len(journal) == 0: # if it was not find in SCIE, checks SSCI
        rfssciFile = open(rfssci)
        data = json.load(rfssciFile)
        rfssciFile.close()
        for i in data:
            if 'issn' in i and i['issn'] == issn:
                journal.append({'journalImpactFactor': i['journalImpactFactor'],
                                'articleInfluenceScore': i['articleInfluenceScore'],
                                'categoryName': i['categoryName']})

    z = getClass(journal, data, container_title, issn, 'journalImpactFactor', INFO) # a potential classification

    if len(journal) > 0 and journal[0]['articleInfluenceScore'] > 0:
        q = getClass(journal, data, container_title, issn, 'articleInfluenceScore', INFO)
        # the other potential classification
        if q[2] <= z[2] and q[3] <= z[3]: # keep the best classification
            z = q
    z = z[:2] # remove auxiliary variables v and vi
    if z[0] == '' and wos != '':
        z[0] = 'ISI ESCI'
    if INFO and z[1] == 'D': # check if it is in Scopus
        a = CurlHelper()
        if 'scopus' in json.dumps(a.getScopusObject(doi)):
            z[1] = 'C'
    os.chdir('..')
    return z


def getClassCNATDCUFromPaperConference(isInWos, doi):
    if isInWos:
        return "ISI PROC"
    elif doi.split("/")[0] == "10.1109":
        return "IEEE PROC"
    else:
        return ""

def getAcronym(container_title):
    if '[' in container_title and ']' in container_title:
        return(container_title.split("[")[1].split("]")[0]) # eg: [CSAE]
    elif '(' in container_title and ')' in container_title:
        return(container_title.split("(")[1].split(")")[0]) # eg: (CSAE)
    elif '-' in container_title:
        return(container_title.split('-')[1].split(' ')[1]) #eg: - CSAE '18
    else:
        if container_title.split()[-1].isnumeric():
            return(container_title.split()[-2] + ' '+ container_title.split()[-1]) # eg: CSAE 2019
        else:
            return(container_title.split()[-1]) # eg: CSAE

def getCOREClassification(acronym, title, year):
    classInfo= 'D'
    coreYears = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk('Files'):
        for file in f:
            if 'CORE' in file:
                coreYears.append(int(file[file.find('-')+1:-4]))
    #sort the CORE years and keep the year of the publication or the closest lower year than the publication
    coreYear = sorted(coreYears, key=lambda x: abs(x - year))[0] #to know in which CORE file to look at
    with open('Files/CORE-'+str(coreYear)+'.csv', newline='') as csvfile:
        csvReader = csv.reader(csvfile, delimiter=',')
        for row in csvReader:
            # match ratio over conference title and match the acronym to its respective cell
            if acronym == row[2] and (SequenceMatcher(None, title.lower(), row[1].lower()).ratio() >= 0.50):
             classInfo = row[4]
    #no classification pre 2008 so keep it as 'D'
    if(year<2008):
        return 'D'
    else:
        return classInfo

def getClassINFO(publisher, publisherLocation, year):
    sheet = openpyxl.load_workbook('Files/SENSE.xlsx').active
    entries = {}
    for row in sheet.iter_rows():
        if(row[2].value and row[3].value):
         #check location and publisher
         if(publisher.lower() in row[2].value.lower() and publisherLocation.lower() in row[3].value.lower()):
             #dictionary of years and their classifications
            entries[row[4].value] = row[5].value
    if entries:
        #the exact year or closest lower year in the dictionary
        coreYear = sorted(entries.keys(), key=lambda x: abs(x - year))[0]
        return entries[coreYear]
    else:
        return ""

def getInformationOfPaper(doi, INFO=False, logger=Logger()):
    wos = curlHelper.getWOS(doi)
    logger.info("wos={}".format(wos))
    outputText = curlHelper.getOutputText(doi)
    logger.info("outputText={}".format(json.dumps(outputText, indent=4, sort_keys=True)))  # pretty print
    year = 0
    if 'published-print' in outputText:
        year = outputText['published-print']['date-parts'][0][0]
    elif 'published-online' in outputText:
        year = outputText['published-online']['date-parts'][0][0]
    else:
        logger.error("For object with doi={} and data={} no year was found".format(doi, outputText))
    logger.info("year={}".format(year))
    paperType = outputText['type']
    logger.info("type={}".format(paperType))
    classCNATDCU = None
    classINFO = None

    if paperType == 'article-journal':
        issn = outputText['ISSN']
        logger.info("issn={}".format(issn))
        container_title = outputText['container-title']
        logger.info("container_title={}".format(container_title))
        li = getClassCNATDCUFromArticleJournal(INFO, issn, year, container_title, wos, doi)
        classCNATDCU = li[0]
        classINFO = li[1]
    elif paperType == 'paper-conference':
        event = outputText['event']
        container_title = outputText['container-title']
        logger.info("event={}".format(event))
        acronym = getAcronym(container_title)
        logger.info("acronym={}".format(acronym))
        if "[" in event and "]" in event:
            event_title = event.split("[")[0] + event.split("]")[1]
        else:
            event_title = event
        logger.info("event_title={}".format(event_title))
        classINFO = getCOREClassification(acronym,event_title ,year) #IRI
        classCNATDCU = getClassCNATDCUFromPaperConference(wos is not None, doi)
    elif paperType == 'book' or paperType == 'chapter':
        publisher = outputText['publisher']
        logger.info("publisher={}".format(publisher))
        publisher_location = ""
        if 'publisher-location' in outputText:
            publisher_location = outputText['publisher-location']
        else:
            logger.error("For object with doi={} and data={} no publisher-location was found".format(doi, outputText))
        logger.info("publisher_location={}".format(publisher_location))
        if INFO:
            classINFO = getClassINFO(publisher, publisher_location, year)

    return {
        "type": paperType,
        "classCNATDCU": classCNATDCU,
        "WoS": wos,
        "classINFO": classINFO if INFO else None}


def main():
    if len(sys.argv) == 3:
        if sys.argv[2].lower() == 'yes' or sys.argv[2].lower() == 'y':
            INFO = True
        elif sys.argv[2].lower() == 'no' or sys.argv[2].lower() == 'n':
            INFO = False
        else:
            print('incorrect input')
            exit(1)
        # paperInfo = getInformationOfPaper(sys.argv[1], INFO)
        print(getInformationOfPaper(sys.argv[1], INFO))
    else:
        # article
        INFO = True
        # paperInfo = getInformationOfPaper('10.1007/s12532-017-0130-5', INFO)
        # paperInfo = getInformationOfPaper('10.1145/3158661', logger=showInfoLogger)
        # paperInfo = getInformationOfPaper('10.1109/JSYST.2018.2805293', INFO) #, logger=showInfoLogger)
        # paper-conference
        # paperInfo=getInformationOfPaper('10.1145/3331453.3361677')
        os.chdir('Files')
        f = open("Test-Data.txt", "r")
        zx = f.read().split('\n')
        f.close()
        os.chdir('..')
        s = 0
        for i in zx:
            s += 1
            print(str(s) + ' out of ' + str(len(zx)))
            print(getInformationOfPaper(i, INFO))  # , logger=showInfoLogger))
        # book
        # paperInfo=getInformationOfPaper('10.1007/978-3-319-30866-1')

        # chapter
        # paperInfo=getInformationOfPaper('10.1007/978-3-319-30866-1_1')

        # print(paperInfo)


if __name__ == '__main__':
    try:
        main()
    except CoconutException as e:
        print(e)
