class Logger:
    def __init__(self, showDebug=False, showInfo=False, showWarn=False, showError=True):
        self.showDebug = showDebug
        self.showInfo = showInfo
        self.showWarn = showWarn
        self.showError = showError

    def debug(self, message):
        if self.showDebug:
            print("DEBUG: " + message)

    def info(self, message):
        if self.showInfo:
            print("INFO: " + message)

    def warn(self, message):
        if self.showWarn:
            print("WARN: " + message)

    def error(self, message):
        if self.showError:
            print("ERROR: " + message)
