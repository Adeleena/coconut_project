class CoconutException(Exception):
    pass


class JSONException(CoconutException):
    @staticmethod
    def formatMessage(text):
        return "Text could not be parsed. Text: {}".format(text)


class RequestException(CoconutException):

    @staticmethod
    def formatMessage(url):
        return "Request failed for url={}".format(url)
